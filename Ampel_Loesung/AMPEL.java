public class AMPEL extends SPIEL{
  
  GEHAEUSE gehaeuse;
  LAMPE lampeOben;
  LAMPE lampeUnten;
  
  public AMPEL(){
    super();
    gehaeuse = new GEHAEUSE();

    lampeOben = new LAMPE();
    lampeOben.setzeMittelpunkt(250,150);
    lampeOben.setzeRadius(55);
    lampeOben.setzeFarbe("rot");

    lampeUnten = new LAMPE();
    lampeUnten.setzeMittelpunkt(250,300);
    lampeUnten.setzeRadius(55);
    lampeUnten.setzeFarbe("gruen");

  }
}