/**
Eine Lampe wird als Kreis dargestellt.
*/
public class LAMPE extends KREIS{
  public LAMPE(){
    super();
  }
  public void an(String farbe){
    farbeSetzen(farbe);
  }
  public void aus(){
    farbeSetzen("schwarz");
  }
}